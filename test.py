#!/usr/bin/python

import drubot 
  
robot=drubot.DruBot() 
robot.forward(0.5) 
robot.backward(0.5) 
robot.right(runSec=0.5) 
robot.left(runSec=0.5) 
robot.backright(runSec=0.5) 
robot.backleft(runSec=0.5)

print "\nCleaning up gpio..."
robot.gpioCleanup
print "Done."
