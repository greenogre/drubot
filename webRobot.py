#!/usr/bin/python

import web
#import runRobot
import move

 
urls = (
  "/runrobot", move.app_move,
  "/(.*)", "index"
)

class index:
    def GET(self, path):
        return "hello " + path

app = web.application(urls, globals())

if __name__ == "__main__":
    app.run()
    