#!/usr/bin/python

import subprocess, re
  

lsblk = subprocess.Popen(['lsblk', '-ln'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
#lsblk = subprocess.check_output("cat lsblk.dat", shell=True)
#lsblk = subprocess.Popen(['lsblk'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
output = lsblk.stdout
diskList = {}
lineNum=0
for line in output: # lsblk.stdout:
    #pline = pprint.pformat( line )
    if 'disk' in line or 'part' in line:
        parts = re.split(r'\s+', line.strip())
        name, majmin, rm, size, ro, devtype = parts[:6]
        if len(parts) > 6:
            mountpoint = parts[6]
        else:
            mountpoint = None
        diskList[lineNum] = (name, devtype, size, rm )
        #print( majmin )
        lineNum += 1
        
for disk in range(lineNum):
    print diskList[disk]

returncode = lsblk.wait()
if returncode:
    print( "things got bad. real bad.", returncode )

