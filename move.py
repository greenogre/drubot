import web
import runRobot

runRobot = runRobot.runRobot()

urls = (
  "", "reblog",
  "/runrobot", "move"
)

### Templates
t_globals = {
    'datestr': web.datestr
}

render = web.template.render('templates', base='run', globals=t_globals)

class reblog:
    def GET(self): raise web.seeother('/runrobot')


class move:
    
    form = web.form.Form(
        web.form.Textbox('runTime:', web.form.notnull, 
            size=2,
            description="Run time:"),
        web.form.Button('Go Forward'),
    )
    
    def GET(self):
        return "Yo!"
        form = self.form()
        return render.new(form)
        
    def POST(self):
        form = self.form()
        if not form.validates():
            return render.new(form)
        runRobot.run(self.runTime,100,0)
        raise web.seeother('/runrobot')
    
    
class moveOld:
    def GET(self, path):
        if path == 'forward':
            runRobot.run(1,100,0)
            message = "Robot moving " + path + "\n"
        elif path == 'backward':
            runRobot.run(1,-100,0)
            message = "Robot moving " + path + "\n"
        elif path == 'left':
            runRobot.run(0.75,100,-100)
            message = "Robot turning " + path + "\n"
        elif path == 'right':
            runRobot.run(0.75,100,100)
            message = "Robot turning " + path + "\n"
        else:
            message = "ERROR: Unknown direction \"" + path + "\"\n"
        return message

app_move = web.application(urls, locals())
