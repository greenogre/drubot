#!/usr/bin/python

import time
#from fractions import Fraction

#motorNum=0
#power='100'
#powerF = float(power)
#powerS = str(round(powerF/100.0,2))
#wr = Fraction(powerS)    
#print "Motor:", motorNum, ":", powerF, powerS, pwr, pwr.numerator, pwr.denominator
#powerFactor = pwr.numerator
#powerCycle = pwr.denominator
    
#powerStep = float(step)
#print "Motor:", motorNum, "Power:", power, "PF:", powerFactor,"PC:", powerCycle
pwrPercent = 75
pwmMotorMinStep = 0.00014 # 0.000028 at 100 res # MinStep is motor time constant * 5. Motor time constant = L/R (impedance/resistance)
pwmResolution = 20 #(each Period made up of pwmResolution steps)
# Minstep = 0.000028, Res = 100
# Minstep = 0.00014, Res = 20

pwmPeriod = pwmMotorMinStep * pwmResolution

pwrCut = int(float(pwmResolution) / 100 * pwrPercent) # Cut off power after pwrCut steps in each cycle

pwmStep = pwmMotorMinStep
pwmStepAdjusted = pwmMotorMinStep # * 0.7 # 0.968

sleepOn = pwmMotorMinStep * pwrCut
sleepOff = pwmMotorMinStep * (pwmResolution - pwrCut)
              
start = time.time()

motorOn = 1
time.sleep(sleepOn)
motorOn = 0
time.sleep(sleepOff)

stop = time.time()

runtime = stop - start
realStep = round(runtime/pwmResolution, 6)

print
print " pwmMotorMinStep", pwmMotorMinStep
print "   pwmResolution", pwmResolution
print "      pwmPeriod:", pwmPeriod
print "   pwmFrequency:", 1 / pwmPeriod, "Hz"
print "pwmStepAdjusted:", pwmStepAdjusted
print "         pwrCut:", pwrCut
print
print "        Runtime:", round(runtime, 6)
print "Configured step:", float(pwmPeriod) / pwmResolution
print "    Actual step:", realStep
print "       Overhead:", round((realStep-pwmStep)/realStep*100,2), "%"
#import drubot 
  
#robot=drubot.DruBot() 
#robot.forward(0.5) 
#robot.backward(0.5) 
#robot.right(runSec=0.5) 
#robot.left(runSec=0.5) 
#robot.backright(runSec=0.5) 
#robot.backleft(runSec=0.5)

#print "\nCleaning up gpio..."
#robot.gpioCleanup
#print "Done."
