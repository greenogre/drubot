# drubot
A Raspberry Pi program to control a simple robot with 2 DC motors through a L293D chip.

Originally based on "ArthurBot" by Andrew Oakley. You can find Andrew's project and instructions
on building the motor driver hardware for the Raspberry Pi at:
http://www.aoakley.com/articles/2013-09-19-raspberry-pi-lego-robot-part1.php

DruBot has motor speed control and the rate of turn can be set as well as the duration.

Latest version is now "runRobot".
runRobot utilizes threading to run each motor on different power cycles.

Currently using a "linear" model for the power control with vaying frequency of short pulses is not very effective.
Will be trying pulse WIDTH modulation and a less linear approach to the power percentage.