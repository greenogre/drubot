#!/usr/bin/python

from datetime import datetime
import time

#print "Startingi up..."

def check_sleep(amount):
    #print "run:", i
    start = datetime.now()
    time.sleep(amount)
    end = datetime.now()
    delta = end-start
    return delta.seconds + delta.microseconds/1000000.

#print "running loop..."
error = sum(abs(check_sleep(0.00028)-0.00028) for i in xrange(100))*10
print "Average error is %0.2fms" % error

