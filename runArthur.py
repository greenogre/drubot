#!/usr/bin/python

import runRobot 

runRobot=runRobot.runRobot()
  
def forward(runtime=1, power=100, turn=0):
    runRobot.run(runtime, power, turn)
def backward(runtime=1, power=-100, turn=0):
    runRobot.run(runtime, power, turn)
def left(runtime=1, power=-100, turn=0):
    runRobot.run(runtime=0.75, power=100, turn=-100) 
def right(runtime=1, power=-100, turn=0):
    runRobot.left(runtime, power, turn) 

runRobot.gpioCleanup
print "Done."
