#!/usr/bin/python

import optparse, time, threading
# from fractions import Fraction


# import RPi.GPIO as gpio (if it is available)
try:
    import RPi.GPIO as gpio
    gpioAvailable = True
except ImportError:
    gpioAvailable = False

#### Constants ####

# If Sim Mode is set, then instead of the GPIO pins being
# turned on/off, the program will instead just print messages
# to the system standard output. Handy for practice programs.
# This constant defines whether sim mode is the default or not.
# You can override this when instantiating the runRobot object.
_DEFAULT_SIM_MODE=False

# Two motors with two directions each = four GPIO pins
# The L293D chip also has an "enable" pin - a dead man's handle;
# if the enable pin is off, the other pins are ignored.
# Here we define which GPIO pins do what
_GPIO_PIN_LEFT_MOTOR_FORWARD=25   # Physical 22
_GPIO_PIN_LEFT_MOTOR_REVERSE=24   # Physical 18 
_GPIO_PIN_LEFT_MOTOR_ENABLE=23    # Physical 16
_GPIO_PIN_RIGHT_MOTOR_FORWARD=4   # Physical 7
_GPIO_PIN_RIGHT_MOTOR_REVERSE=17  # Physical 11
_GPIO_PIN_RIGHT_MOTOR_ENABLE=22   # Physical 15

_GPIO_PIN_SONAR_TRIGGER=18        # Physical 12
_GPIO_PIN_SONAR_ECHO=27           # Physical 13

# Vcc +5v to motor controller     # Physical 2
# Vcc +5v to sonar                # Physical 4
# Grnd to sonar                   # Physical 6

# GPIO pin values in arrays for multi-threaded motor instances
_MOTOR_FORWARD = [ _GPIO_PIN_LEFT_MOTOR_FORWARD, _GPIO_PIN_RIGHT_MOTOR_FORWARD ]
_MOTOR_REVERSE = [ _GPIO_PIN_LEFT_MOTOR_REVERSE, _GPIO_PIN_RIGHT_MOTOR_REVERSE ]
_MOTOR_ENABLE = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE ]

#### Global Variables ####

# These variables are shared with all functions and sub-processes

global RUNTIME
global POWER
global POWERSTEP
global DEBUG

# Set default values

# Default motor runTime
_DEFAULT_RUNTIME = '1'
# Default power setting
_DEFAULT_POWER = '100'
# Length of each step in motor run functions (** EXPERTS ONLY **)
_DEFAULT_POWERSTEP = '0.001'

# PWM variables
# MotorMinStep is the minimum time it is worth turning the motor on for
# It is calculated as the motor time constant * 5. Motor time constant = L/R (impedance/resistance)
# We can't actually go that low due to language performance limitations so it is the minimum step
# that we can realistically use. 
_PWM_MIN_STEP = '0.00014'
_PWM_PULSE_RESOLUTION = '20' # e.g. 20 steps of _PWM_MIN_STEP = 1 cycle



# Set practical max/min values
_MIN_RUNTIME = round( float(_PWM_MIN_STEP) * float(_PWM_PULSE_RESOLUTION), 3) * 2
_MIN_POWER = '5'
_MAX_POWER = '100'

# Set defaults for global vars
# Default runTime
RUNTIME = _DEFAULT_RUNTIME
# Default power setting ( L, R )
POWER = _DEFAULT_POWER
# Length of each step in motor run functions (** EXPERTS ONLY **)
POWERSTEP = _DEFAULT_POWERSTEP
# Enable debugging messages if True
DEBUG = False

#### One-time setup ####

# Set up the GPIO pins, referring to the constants
if gpioAvailable:
    gpio.setwarnings(False)
    gpio.setmode(gpio.BCM)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_FORWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_REVERSE, gpio.OUT)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_ENABLE, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_FORWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_REVERSE, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_ENABLE, gpio.OUT)

#### Objects ####

class runRobot(object):
    # We need to keep a track of instances,
    # since we should only ever have one.
    # You could adapt this program to have more
    # - the Raspberry Pi has enough GPIO pins -
    # but you'd need to change this code a lot.
    _instances=[]
    
    def __init__(self,simMode=_DEFAULT_SIM_MODE):
        if ( len(self._instances)>1 ):
            print "ERROR: You can't have more than one runRobot instance."
            exit(1)
        self._instances.append(self)
        if ( gpioAvailable == False ):
            self.simMode=True
            print "\nNOTICE: RPi.GPIO not available."
        else:
            self.simMode=simMode
        if ( self.simMode ):
            print "Running in Sim Mode.\n"
            
    # Stop all motors and reset control pins
    def allStop(self):
        if self.simMode:
            print "Stop all motors and reset."
        else:
            pinList = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE, _GPIO_PIN_LEFT_MOTOR_FORWARD, _GPIO_PIN_LEFT_MOTOR_REVERSE, _GPIO_PIN_RIGHT_MOTOR_FORWARD, _GPIO_PIN_RIGHT_MOTOR_REVERSE ]
            gpio.output( pinList, 0)
        
    # You should call this function to clean up the GPIO state just before the program terminates.
    def gpioCleanup(self):
        if self.simMode:
            print "Clean up GPIO."
        else:
            pinList = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE, _GPIO_PIN_LEFT_MOTOR_FORWARD, _GPIO_PIN_LEFT_MOTOR_REVERSE, _GPIO_PIN_RIGHT_MOTOR_FORWARD, _GPIO_PIN_RIGHT_MOTOR_REVERSE ]
            gpio.cleanup( pinList )
            
    # Check motor runtime is above minimum
    def minRunCheck(self, secs, Min=_MIN_RUNTIME):
        seconds = float(secs)
        runSecs=abs(seconds)
        minimum = float(Min)
        if runSecs < abs(minimum):
            if DEBUG:
                print "WARNING: Requested run time \"{0}\" < minimum. Using minimum {1} seconds.".format(runSecs,Min)
            return minimum
        else:
            return runSecs
        
    # Check rate value is between Min and Max and fix if necessary (Note: Rate value of 0 is OK)
    def rateCheck(self, Rate, Min=_MIN_POWER, Max=_MAX_POWER, Name=""):
        minimum = int(Min)
        maximum = int(Max)
        if Name:
            Name = " in " + Name        
        if abs(int(Rate)) > 0 and abs(Rate) < abs(minimum):
            if DEBUG:
                print "WARNING{0}: Requested rate {1}% < minimum. Using minimum {2}%.".format(Name,Rate,Min)
            return minimum*cmp(Rate,0)
        elif abs(Rate) > abs(maximum):
            if DEBUG:
                print "WARNING{0}: Requested rate {1}% > maximum. Using maximum {2}%.".format(Name,Rate,Max)
            return Max*cmp(Rate,0)
        else:
            return Rate
            
    # Calculate the ratio of inside/outside power rates for given turn rate
    # 100 = tightest right, -100 = tightest left, 0 = go straight
    # returns factor from -1 to 1
    def diffRatio(self, turnRate):
        if turnRate != 0:
            absRate = abs(turnRate)
            rate = self.rateCheck(absRate, 0, Name="diffRatio")
            ratio = (100 - ( rate * 2)) / 100
            if DEBUG:
                print "Turn Ratio:", ratio # round(ratio,2)
            #return round(ratio,2) * cmp(turnRate, 0)
            return round(ratio,2)
        else:
            return 1
        
    #Function to control motors
    def runMotor(self,motorNum=0, runTime=1, power=100, pwmStep=_PWM_MIN_STEP, pwmRes=_PWM_PULSE_RESOLUTION):
        #print "Motor runtime:", motorNum, ":", RUNTIME
        runFor = abs(runTime)
        if runFor < _MIN_RUNTIME:
            runFor = _MIN_RUNTIME
            
        # Set GPIO for motor direction
        motorSpin = cmp(power, 0)
        if self.simMode:
            print "Motor:", motorNum, "MF:", (motorSpin**2 + motorSpin)/2, "MR:", (motorSpin**2 - motorSpin)/2
        else:
            gpio.output(_MOTOR_FORWARD[motorNum], (motorSpin**2 + motorSpin)/2 )
            gpio.output(_MOTOR_REVERSE[motorNum], (motorSpin**2 - motorSpin)/2 )
        
        # print "Motor power:", motorNum, ":", power
        
        pwrCut = int(float(pwmRes) / 100 * abs(power)) # Delay before cutting off power to motor
        
        sleepOn = (float(pwmStep) * pwrCut)
        sleepOff = float(pwmStep) * ( float(pwmRes) - pwrCut)
        
        start = time.time()
        stop = start + runFor
        if self.simMode:
            print "Motor:", motorNum, "pwm_run..."
            while time.time() <= stop:
                motorState = 1
                time.sleep(sleepOn)
                motorState = 0            
                time.sleep(sleepOff)
        else:
            while time.time() <= stop:
                gpio.output(_MOTOR_ENABLE[motorNum], 1)
                time.sleep(sleepOn)            
                gpio.output(_MOTOR_ENABLE[motorNum], 0)
                time.sleep(sleepOff)
        
        return motorNum, "Done"
    
    def run(self, runTime, power, turn):
        rntm = self.minRunCheck(runTime)
        pwr = self.rateCheck(power, Name="run.pwr")
        trn = self.rateCheck(turn, 0, Name="run.trn")
        turnDir = cmp(trn, 0)
        if turnDir < 0:
            Power = [ pwr * self.diffRatio(trn), pwr ]
        elif turnDir > 0:
            Power = [ pwr, pwr * self.diffRatio(trn) ]
        else:
            Power = [ pwr, pwr ]
        
        # ToDo: Set motorSpin
        threads = []
        for i in range(2):
            if DEBUG:
                print "Motor:", i, "Power:", Power[i]
            
            Power[i] = self.rateCheck(Power[i], Name="runThread_" + str(i))
            
            t = threading.Thread(target=self.runMotor, args=(i, rntm, Power[i]))
            threads.append(t)
            t.start()

    
    def clParser(self):
        # Declare any global vars we will modfiy here
        global POWER
        global DEBUG
        
        #Create instance of OptionParser Module, included in Standard Library
        p = optparse.OptionParser(description='Robot Runner',
                                                prog='drubot',
                                                version='drubot 0.3.0',
                                                usage='%prog [option]')
        p.add_option('--runtime', '-r', action="store", type='str', dest='runtime', default=_DEFAULT_RUNTIME, help='Time to run motors (in seconds) [Default = 1]', metavar="SECONDS")
        p.add_option('--power', '-p', action="store", type='str', dest='power', default=_DEFAULT_POWER, help = 'Motor power (in %), + = forward, - = reverse [Default = 100]', metavar="[-]POWER")
        p.add_option('--turn', '-t', action="store", type='str', dest='turn', default='0', help='Turn rate -100 to 100, + = right, - = left', metavar="[-]TURN")
        p.add_option('--simulate', '-s', action="store_true", dest='simMode', default=False, help='Run in Sim Mode. Do not take actions, just print messages')
        p.add_option('--debug', '-d', action="store_true", dest='debug', help='Print debugging information.')
        #Option Handling passes correct parameter to runBash
        options, arguments = p.parse_args()
        if options.runtime:
            rntm = float(options.runtime)
            #print "runtime:", rntm
            clRunTime = self.minRunCheck(rntm, 0.2)
            #print "runtime:", RUNTIME
        if options.turn:
            trn = float(options.turn)
            if int(trn) != 0:
                clTurn = self.rateCheck(trn, 10, Name='clParser.turn')        
            else:
                clTurn = 0
            # print "turnRate:", clTurn
        if options.power:
            pwr = float(options.power)
            clPower = self.rateCheck(pwr, 5, Name='clParser.pwr')
        if options.simMode:
           if gpioAvailable: 
               self.simMode = True
        if options.debug:
            DEBUG = True
        if DEBUG:
            print "Command line arguments:", arguments
            
        self.run(clRunTime, clPower, clTurn)
        # ToDo:
        # Check that all runMotor threads are finished then
        #    self.allStop()
        #    self.gpioCleanup
                                
#Runs all the functions
def main():
    
    robot=runRobot()
    robot.clParser()

#This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    main()
