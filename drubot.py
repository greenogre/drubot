#!/usr/bin/env python
# DruBot, a module for controlling an L293D-based robot 
# from a Raspberry Pi.
# Andrew Oakley www.aoakley.com
# Public Domain 2013-09-13

# The L293D chip typically controls two DC motors.
# An L293D-based robot typically has two tracks, like
# an army tank. Alternatively it may have two drive wheels
# and one or more free-spinning castors or skis/skids, like
# a Logo turtle. Either way, it has two motors, one on the
# left and one on the right.
# Think of a BigTrak toy, Johnny Five from "Short Circuit"
# or a bomb disposal robot.

# Each motor can spin forward or backward.
# Turning left or right is achieved by having one side
# go forward whilst the other goes backward, which
# spins the robot around like a tank.

# The robot may also speak (using the espeak Linux program)
# and take a photo (using the Raspberry Pi camera)

#### Imports ####

# Import prerequisite modules
#import time, sys, tempfile, os, string, shutil
import time

# import RPi.GPIO as gpio (if it is available)
try:
    import RPi.GPIO as gpio
    gpioAvailable = True
except ImportError:
    gpioAvailable = False


#### Constants ####

# Here's where you can fine-tune how this module works, without
# messing up the program itself.

# If Dry Run Mode is set, then instead of the GPIO pins being
# turned on/off, the program will instead just print messages
# to the system standard output. Handy for practice programs.
# This constant defines whether dry run mode is the default or not.
# You can override this when instantiating the DruBot object.
_DEFAULT_DRY_RUN_MODE=False

# Two motors with two directions each = four GPIO pins
# The L293D chip also has an "enable" pin - a dead man's handle;
# if the enable pin is off, the other pins are ignored.
# Here we define which GPIO pins do what
_GPIO_PIN_LEFT_MOTOR_FORWARD=25   # Physical 22
_GPIO_PIN_LEFT_MOTOR_BACKWARD=24  # Physical 18 
_GPIO_PIN_LEFT_MOTOR_ENABLE=23    # Physical 16
_GPIO_PIN_RIGHT_MOTOR_FORWARD=4   # Physical 7
_GPIO_PIN_RIGHT_MOTOR_BACKWARD=17 # Physical 11
_GPIO_PIN_RIGHT_MOTOR_ENABLE=22   # Physical 15

_GPIO_PIN_SONAR_TRIGGER=18        # Physical 12
_GPIO_PIN_SONAR_ECHO=27           # Physical 13

# Vcc +5v to motor controller     # Physical 2
# Vcc +5v to sonar                # Physical 4
# Grnd to sonar                   # Physical 6

# Next we define how long a "step" is. A step is defined
# in terms of milliseconds (thousandths of a second).
# So if a step is 500 milliseconds, then commanding the
# robot to go forward for 2 steps would result in both
# motors being turned on in the forward direction for
# 1 second (2x500ms) and then turned off.
# Note that neither Python nor Raspbian are "real-time", so
# it's pointless trying to be accurate to less than ten
# milliseconds or so.
# Step duration is used for forward and backward commands.
_STEP_DURATION=1000

# Now the turn duration. This is used for the left and right
# commands. Again, this is milliseconds. You could fine-tune
# this so it matches a quarter-turn, or 45 degrees, or
# whatever suits your project. I doubt you'll get much finer
# control than approximately 15 degrees. If you can hand-build
# a robot that can travel in a perfect square, well done you!
_TURN_DURATION=750

# "Bias" is a tweak for where you have one motor (or
# gearing) which is slightly stronger than the other - which
# is my case using very old 1970s/1980s Lego motors. This
# results in repeated forward steps actually turning
# slightly off to one side. We will compensate by letting the
# left motor run for slightly shorter or longer than the
# right motor. The right motor will always run for DURATION,
# whilst the left motor will run for DURATION+BIAS.
# Don't have the bias greater than the duration - your robot
# will drag itself around in circles.
# Bias is applied to backward, left and right too.
# Veering right? Use a negative value.
# Veering left? Use a positive value.
# Value is in milliseconds. Try 100 for starters.
_STEP_BIAS=0
_TURN_BIAS=100

# That's the end of the configuration. You should not need
# to change anything past this point. Of course, you *can*
# change anything you like, but changing things past this
# point makes it much more likely to go wrong. In particular,
# make sure you always turn the motors off at the end - 
# otherwise your precious buddy might throw himself off a
# table, down some stairs, or against a wall.

#### One-time setup ####

# Set up the GPIO pins, referring to the constants
if gpioAvailable:
    gpio.setwarnings(False)
    gpio.setmode(gpio.BCM)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_FORWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_BACKWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_ENABLE, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_FORWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_BACKWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_ENABLE, gpio.OUT)

#### Objects ####

class DruBot(object):
    # We need to keep a track of instances,
    # since we should only ever have one.
    # You could adapt this program to have more
    # - the Raspberry Pi has enough GPIO pins -
    # but you'd need to change this code a lot.
    _instances=[]

    # Initialise the object
    def __init__(self,dryRunMode=_DEFAULT_DRY_RUN_MODE):
        if ( len(self._instances)>1 ):
            print "ERROR: You can't have more than one DruBot instance."
            exit(1)
        self._instances.append(self)
        if ( gpioAvailable == False ):
            self.dryRunMode=True
            print "RPi.GPIO not available."
        else:
            self.dryRunMode=dryRunMode
        if ( self.dryRunMode ):
            print "Running in Dry Run Mode.\n"

    
    ## Replicate ArthurBot function ##

    # Move forward so many seconds at Rate% power
    def forward(self,runSec=1,Rate=100):
        self.straight(runSec,abs(Rate))

    # Move backward so many seconds at Rate% power
    def backward(self,runSec=1,Rate=-100):
        self.straight(runSec,abs(Rate)*-1)
           
    # Turn right at turn rate for so many seconds at % power 
    def right(self,turnRate=100,runSec=0.75,Rate=100):
        self.turn(turnRate,runSec,Rate)
        
    # Turn left at turn rate for so many seconds at % power 
    def left(self,turnRate=-100,runSec=0.75,Rate=100):
        self.turn(turnRate,runSec,Rate)
    
    # Turn right going backwards at turn rate for so many seconds at % power 
    def backright(self,turnRate=100,runSec=0.75,Rate=-100):
        self.turn(turnRate,runSec,abs(Rate)*-1)
        
    # Turn left at turn rate for so many seconds at % power 
    def backleft(self,turnRate=-100,runSec=0.75,Rate=-100):
        self.turn(turnRate,runSec,abs(Rate)*-1)
    
    ## New functions for straight and turn ##
    
    # Move forwards or backwards (negative Rate) for so many seconds at Rate% power
    def straight(self,runSec=1,Rate=100):
        print "Rate:", Rate
        runFor=self.minRunCheck(runSec,0.3)
        checkedRate=self.rateCheck(Rate,5,100)
        print "checkedRate:", checkedRate
        # Check abs Rate is between 5 and 100 
        if ( self.dryRunMode ):
            if cmp(checkedRate,0) > 0:
                direction=" forward"
            else:
                direction="backward"
            print "Going {0} for {1} seconds at {2}% power...".format(direction,runFor,checkedRate)
            time.sleep(runFor)
            #print "Done."
        else:
            #t0 = time.time()
            #print t0
            startTime = time.time()
            runTill = startTime + float( runFor )
            #runTill=time.clock() + runFor
            scaledRate=int( round( checkedRate / 10 ) )
            runRate = ( 11 - abs(scaledRate) ) * cmp(scaledRate, 0)
            #print "startTime: %f :\nrunFor: %f :\nrunTill: %f :\n" % ( startTime, runFor, runTill)
            while time.time() < runTill:
                self.runMotors(checkedRate,checkedRate)
                #print ".",
            #print "stopTime: %f :" % time.time()
            #print time.time() - t0
            self.allStop
    # turnRate [required] is sharpness of turn, 10-100 (higher = sharper)
    # Turn for runSec seconds.
    # Rate is % power 
    # Positive turnRate = Right (Clockwise if forward)
    # Negative turnRate = Left (Anti-clockwise if forward)
    def turn(self,turnRate=50,runSec=0.75,Rate=100):
        checkedTurnRate=abs(self.rateCheck(turnRate,10,100))
        runFor=self.minRunCheck(runSec)
        outsideRate=self.rateCheck(Rate,10,100)
        if ( self.dryRunMode ):
            if cmp(outsideRate,0) > 0:
                direction=" forward"
            else:
                direction="backward"
            if cmp(turnRate,0) > 0:
                turn="right"
            else:
                turn=" left"
            print "Going {0}, turning {1} at rate {2}% for {3} seconds at {4}% power...".format( direction, turn, checkedTurnRate, runFor, outsideRate)
            time.sleep(runFor)
        else:
            startTime = time.time()
            runTill = startTime + float( runFor )
            insideRate = outsideRate - outsideRate * checkedTurnRate / 50
            if cmp( checkedTurnRate,0 ) > 0:
                leftPower = outsideRate
                rightPower = insideRate
            else:
                leftPower = insideRate
                rightPower = outsideRate
            #print "startTime: %f :\nrunFor: %f :\nrunTill: %f :\n" % ( startTime, runFor, runTill)
            while time.time() < runTill:
                self.runMotors(leftPower,rightPower)
            #print "stopTime: %f :" % time.time()
            self.allStop

    ## Internal functions ##
    
    # Check motor runtime is above minimum
    def minRunCheck(self,secs,Min=0.3):
        runSecs=abs(secs)
        if runSecs < abs(Min):
            print "WARNING: Requested run time \"{0}\" is less than the minimum run time of {1} seconds.".format(runSecs,Min)
            return Min
        else:
            return runSecs
    
    # Check rate value is between Min and Max and fix if necessary
    def rateCheck(self,Rate,Min=0,Max=100):
        if abs(Rate) < abs(Min):
            print "WARNING: Requested motor rate {0}% is less than the minimum of {1}%.".format(Rate,Min)
            return Min*cmp(Rate,0)
        elif abs(Rate) > abs(Max):
            print "WARNING: Requested motor rate {0}% is more than the maximum of {1}%.".format(Rate,Max)
            return Max*cmp(Rate,0)
        else:
            return Rate
        
    # Motor spin mode, 1=forward, 0=stopped, -1=reverse
    def setMotorSpin(self,L,R):
        gpio.output(_GPIO_PIN_LEFT_MOTOR_FORWARD, (L**2 + L)/2 )
        gpio.output(_GPIO_PIN_LEFT_MOTOR_BACKWARD, (L**2 - L)/2 )
        gpio.output(_GPIO_PIN_RIGHT_MOTOR_FORWARD, (R**2 + R)/2 )
        gpio.output(_GPIO_PIN_RIGHT_MOTOR_BACKWARD, (R**2 - R)/2 )
        #print "LF", (L**2 + L)/2
        #print "LR", (L**2 - L)/2
        #print "RF", (R**2 + R)/2
        #print "RR", (R**2 - R)/2
        
    # Run the motors.
    # [Left|Right]MotorPower = 0-100%
    # Positive = forward, Zero = stopped, negative = reverse.
    # numSteps = number of steps for Power Factor (Expert use only!)
    def runMotors(self,LeftMotorPower,RightMotorPower,numSteps=100):
        print "LMP:", LeftMotorPower,":"
        #print "RMP", RightMotorPower
        # Default motors to "off"
        LeftOn=0
        RightOn=0
        LeftOnLast=0
        RightOnLast=0
        
        # Calculate PowerFactor for each motor
        # Check for acceptable value
        checkedLeftPower=abs(self.rateCheck(LeftMotorPower))
        checkedRightPower=abs(self.rateCheck(RightMotorPower))
        # Scale down values 0-100 to 0-10
        scaledLeftRate=int(round(float(checkedLeftPower)/10))
        scaledRightRate=int(round(float(checkedRightPower)/10))
        print "SLR:",scaledLeftRate,":"
        # Invert power value to power factor
        LeftPowerFactor=10/scaledLeftRate
        RightPowerFactor=10/scaledRightRate
        print "LPF:",LeftPowerFactor,":",
        # Set spin mode for each motor
        self.setMotorSpin(cmp(LeftMotorPower,0),cmp(RightMotorPower,0))
        #print "Spin_l_r:",cmp(LeftMotorPower,0),cmp(RightMotorPower,0)
        
        # Run motors at given power factor for ~100ms
        for step in range(0,numSteps):
            print step,":",
            #print float(step) / float(LeftPowerFactor),":",
            leftFloat = float(step) / float(LeftPowerFactor)
            leftInt = float(int(float(step) / float(LeftPowerFactor)))
            rightFloat = float(step) / float(RightPowerFactor)
            rightInt = float(int(float(step) / float(RightPowerFactor)))
            #print leftFloat,leftInt,
            #if (step/LeftPowerFactor)==(int(step/LeftPowerFactor)):
            if leftFloat == leftInt:
                LeftOn=1
            else:
                LeftOn=0
            #if (step/RightPowerFactor)==(int(step/RightPowerFactor)):
            if rightFloat == rightInt:
                RightOn=1
            else:
                RightOn=0
            #if LeftOn!=LeftOnLast:
            gpio.output(_GPIO_PIN_LEFT_MOTOR_ENABLE, LeftOn)
            #if RightOn!=RightOnLast:
            gpio.output(_GPIO_PIN_RIGHT_MOTOR_ENABLE, RightOn)
            print LeftOn,RightOn
            LeftOnLast=LeftOn
            RightOnLast=RightOn
            time.sleep(0.001)
            pinList = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE, ]
            gpio.output( pinList, 0)
    # Stop all motors and reset control pins
    def allStop(self):
        pinList = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE, _GPIO_PIN_LEFT_MOTOR_FORWARD, _GPIO_PIN_LEFT_MOTOR_BACKWARD, _GPIO_PIN_RIGHT_MOTOR_FORWARD, _GPIO_PIN_RIGHT_MOTOR_BACKWARD ]
        gpio.output( pinList, 0)
        #gpio.output( _GPIO_PIN_LEFT_MOTOR_ENABLE, False)
        #gpio.output(_GPIO_PIN_RIGHT_MOTOR_ENABLE, False)
        #gpio.output(_GPIO_PIN_LEFT_MOTOR_FORWARD, False)
        #gpio.output(_GPIO_PIN_LEFT_MOTOR_BACKWARD, False)
        #gpio.output(_GPIO_PIN_RIGHT_MOTOR_FORWARD, False)
        #gpio.output(_GPIO_PIN_RIGHT_MOTOR_BACKWARD, False)
    
    # You should call this function to clean up the GPIO state when the program terminates.
    def gpioCleanup(self):
        if gpioAvailable:
            pinList = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE, _GPIO_PIN_LEFT_MOTOR_FORWARD, _GPIO_PIN_LEFT_MOTOR_BACKWARD, _GPIO_PIN_RIGHT_MOTOR_FORWARD, _GPIO_PIN_RIGHT_MOTOR_BACKWARD ]
            gpio.cleanup( pinList )
