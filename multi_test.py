#!/usr/bin/python

import optparse, time
from fractions import Fraction
from multiprocessing import Pool

# import RPi.GPIO as gpio (if it is available)
try:
    import RPi.GPIO as gpio
    gpioAvailable = True
except ImportError:
    gpioAvailable = False

#### Constants ####

# If Dry Run Mode is set, then instead of the GPIO pins being
# turned on/off, the program will instead just print messages
# to the system standard output. Handy for practice programs.
# This constant defines whether dry run mode is the default or not.
# You can override this when instantiating the DruBot object.
_DEFAULT_DRY_RUN_MODE=False

# Two motors with two directions each = four GPIO pins
# The L293D chip also has an "enable" pin - a dead man's handle;
# if the enable pin is off, the other pins are ignored.
# Here we define which GPIO pins do what
_GPIO_PIN_LEFT_MOTOR_FORWARD=25   # Physical 22
_GPIO_PIN_LEFT_MOTOR_BACKWARD=24  # Physical 18 
_GPIO_PIN_LEFT_MOTOR_ENABLE=23    # Physical 16
_GPIO_PIN_RIGHT_MOTOR_FORWARD=4   # Physical 7
_GPIO_PIN_RIGHT_MOTOR_BACKWARD=17 # Physical 11
_GPIO_PIN_RIGHT_MOTOR_ENABLE=22   # Physical 15

_GPIO_PIN_SONAR_TRIGGER=18        # Physical 12
_GPIO_PIN_SONAR_ECHO=27           # Physical 13

# Vcc +5v to motor controller     # Physical 2
# Vcc +5v to sonar                # Physical 4
# Grnd to sonar                   # Physical 6

#### Global Variables ####

# These variables are shared with all functions and sub-processes

global RUNTIME
global POWERSTEP
global POWER
global SPIN

# Set defaults for global vars
# Default runTime
RUNTIME = 1
# Default power setting ( L, R )
POWER = [ 100, 100 ]
# Default directions for motors ( L, R )
SPIN = [ 1, 1 ]
# Length of each step in motor run functions (** EXPERTS ONLY **)
POWERSTEP = 0.001


#### One-time setup ####

# Set up the GPIO pins, referring to the constants
if gpioAvailable:
    gpio.setwarnings(False)
    gpio.setmode(gpio.BCM)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_FORWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_BACKWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_LEFT_MOTOR_ENABLE, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_FORWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_BACKWARD, gpio.OUT)
    gpio.setup(_GPIO_PIN_RIGHT_MOTOR_ENABLE, gpio.OUT)

#### Objects ####

# Stop all motors and reset control pins
def allStop(self):
    pinList = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE, _GPIO_PIN_LEFT_MOTOR_FORWARD, _GPIO_PIN_LEFT_MOTOR_BACKWARD, _GPIO_PIN_RIGHT_MOTOR_FORWARD, _GPIO_PIN_RIGHT_MOTOR_BACKWARD ]
    gpio.output( pinList, 0)
    
# You should call this function to clean up the GPIO state just before the program terminates.
def gpioCleanup(self):
    if gpioAvailable:
        pinList = [ _GPIO_PIN_LEFT_MOTOR_ENABLE, _GPIO_PIN_RIGHT_MOTOR_ENABLE, _GPIO_PIN_LEFT_MOTOR_FORWARD, _GPIO_PIN_LEFT_MOTOR_BACKWARD, _GPIO_PIN_RIGHT_MOTOR_FORWARD, _GPIO_PIN_RIGHT_MOTOR_BACKWARD ]
        gpio.cleanup( pinList )
        
# Check motor runtime is above minimum
def minRunCheck(self, secs, Min=0.2):
    print "secs:", secs
    runSecs=abs(secs)
    print "runSecs:", runSecs
    if runSecs < abs(Min):
        print "WARNING: Requested run time \"{0}\" < minimum. Using minimum {1} seconds.".format(runSecs,Min)
        return Min
    else:
        return runSecs
    
# Check rate value is between Min and Max and fix if necessary
def rateCheck(self, Rate, Min=0, Max=100):
    if abs(Rate) < abs(Min):
        print "WARNING: Requested rate {0}% < minimum. Using minimum {1}%.".format(Rate,Min)
        return Min*cmp(Rate,0)
    elif abs(Rate) > abs(Max):
        print "WARNING: Requested rate {0}% > maximum. Using maximum {1}%.".format(Rate,Max)
        return Max*cmp(Rate,0)
    else:
        return Rate
        
# Motor spin mode, 1=forward, 0=stopped, -1=reverse
def setMotorSpin(self,L,R):
    gpio.output(_GPIO_PIN_LEFT_MOTOR_FORWARD, (L**2 + L)/2 )
    gpio.output(_GPIO_PIN_LEFT_MOTOR_BACKWARD, (L**2 - L)/2 )
    gpio.output(_GPIO_PIN_RIGHT_MOTOR_FORWARD, (R**2 + R)/2 )
    gpio.output(_GPIO_PIN_RIGHT_MOTOR_BACKWARD, (R**2 - R)/2 )
    
#Function to control motors
def runMotor(self,motorNum=0,):
    print "Motor runtime:", motorNum, ":", RUNTIME
    runFor = abs(RUNTIME)
    if runFor < 0.2:
        runFor = 0.2
    power = POWER[motorNum]
    print "Motor power:", motorNum, ":", power
    powerF = float(power)
    powerS = str(round(powerF/100.0,2))
    pwr = Fraction(powerS)    
    #print "Motor:", motorNum, ":", powerF, powerS, pwr, pwr.numerator, pwr.denominator
    powerFactor = pwr.numerator
    powerCycle = pwr.denominator
    
    if abs(powerFactor) < 1 or powerCycle < 1 or powerFactor > powerCycle:
        powerFactor = powerCycle = 1
        print "Invalid power setting: Using default 100%."
    
    startTime = time.time()
    endTime = startTime + runFor
    # print startTime, endTime
    print "Motor:", motorNum, ": Running at", powerFactor, "/", powerCycle
    while time.time() < endTime:
        if powerCycle == 1:
            motorOn="On"
            print "Motor:", motorNum, "Locked :", motorOn
            time.sleep(runFor)
        else:
            #print "cycle :", powerCycle
            #print "Motor:", motorNum, ": Running at", powerFactor, "/", powerCycle
            for cycle in range(0, powerCycle):
                if cycle < powerFactor:
                    motorOn = "On"
                else:
                    motorOn = "Off"
                # print motorNum, ":", cycle, powerFactor, ":", motorOn
                time.sleep(POWERSTEP)
        #print "time :",time.time()
    motorOn = "Done"
    return motorNum, motorOn

def commandLine():
    
    # Declare any global vars we will modfiy here
    global RUNTIME
    global POWER
    
    
    #Create instance of OptionParser Module, included in Standard Library
    p = optparse.OptionParser(description='Robot Runner',
                                            prog='drubot',
                                            version='drubot 0.3.0',
                                            usage='%prog [option]')
    p.add_option('--runtime', '-r', action="store", type='str', dest='runtime', default='1', help='Time to run motors (in seconds) [Default = 1]', metavar="SECONDS")
    p.add_option('--power', '-p', action="store", type='str', dest='power', default='100', help = 'Motor power (in %), + = forward, - = reverse [Default = 100]', metavar="POWER")
    p.add_option('--turn', '-t', action="store", type='str', dest='turn', default='0', help='Turn rate 0-100, + = right, - = left', metavar="[-]TURN")
    #Option Handling passes correct parameter to runBash
    options, arguments = p.parse_args()
    if options.runtime:
        rntm = float(options.runtime)
        print "runtime_clo:", rntm
        RUNTIME = minRunCheck(0, rntm, 0.2)
        print "runtime_set:", RUNTIME
    if options.turn:
        trn = float(options.turn)
        if int(trn) != 0:
            turnRate=rateCheck(0, trn, 10)
        else:
            turnRate = 0        
        print "turnRate:", turnRate
        if turnRate != 0:
            print "turning"
    if options.power:
        pwr = float(options.power)
        power = rateCheck(0, pwr, 5)
        POWER = [ power, power ]
    #else:
    #    p.print_help()
    print "Run..."

    pool = Pool(5)
    result = pool.map(runMotor, [0,1] )
    #print "Motor :", result[0][0], ":", result[0][1]
    #print "Motor :", result[1][0], ":", result[1][1]
    for motor in range(0,2):
        print "Motor :", result[motor][0], ":", result[motor][1]
        
#Runs all the functions
def main():
    commandLine()

#This idiom means the below code only runs when executed from command line
if __name__ == '__main__':
    main()